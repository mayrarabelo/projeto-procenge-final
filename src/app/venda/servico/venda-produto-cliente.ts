import { VendaProduto } from './venda-produto';
import { Cliente } from 'src/app/cliente/servico/cliente';

export class Venda {
  codigo:string;
  data: Date;
  cliente: Cliente;
  listaVentaItem: VendaProduto[] = [];
}
