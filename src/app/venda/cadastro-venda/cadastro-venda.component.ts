import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Venda } from '../servico/venda-produto-cliente';
import { VendaProduto } from '../servico/venda-produto';
import { VendaService } from '../servico/venda.service';
import { Produto } from 'src/app/produto/servico/produto';
import { ProdutoService } from 'src/app/produto/servico/produto.service';
import { Cliente } from 'src/app/cliente/servico/cliente';
import { ClienteService } from 'src/app/cliente/servico/cliente.service';

@Component({
  selector: 'app-cadastro-venda',
  templateUrl: './cadastro-venda.component.html',
  styleUrls: ['./cadastro-venda.component.css']
})
export class CadastroVendaComponent implements OnInit {

  venda: Venda = new Venda();
  vendaProduto: VendaProduto = new VendaProduto();

  listaProdutos: Produto[] = [];
  listaClientes: Cliente[] = [];

  hoje: number = Date.now();
  operacao: string = 'Incluir';

  constructor(
    private router: Router,
    private vendaService: VendaService,
    private produtoService: ProdutoService,
    private clienteService: ClienteService
  ) { }

  ngOnInit(): void {

    this.clienteService.pesquisar('').subscribe(
      data => {
        this.listaClientes = <Cliente[]>data;
      }
    );

    this.produtoService.pesquisar('').subscribe(
      data => {
        this.listaProdutos = <Produto[]>data;
      }
    );
  }

  incluir(){
    this.vendaService.incluir(this.venda).subscribe(
      data => {
        alert(data['mensagem']);
        this.voltar();
      }
    );
  }

  adicionar(){

    this.venda.listaVentaItem.push(this.vendaProduto);
    this.vendaProduto = new VendaProduto();

  }

  voltar(){
    this.router.navigate(['/venda']);
  }

  removerproduto(vendaProduto){

    this.venda.listaVentaItem = this.venda.listaVentaItem.filter(obj => obj !== vendaProduto);

  }

}
