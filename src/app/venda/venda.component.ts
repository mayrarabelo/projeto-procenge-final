import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Venda } from './servico/venda-produto-cliente';
import { Cliente } from '../cliente/servico/cliente';
import { VendaService } from './servico/venda.service';
import { ClienteService } from '../cliente/servico/cliente.service';

@Component({
  selector: 'app-venda',
  templateUrl: './venda.component.html',
  styleUrls: ['./venda.component.css']
})
export class VendaComponent implements OnInit {

  venda: Venda = new Venda();
  listaVendas: Venda[] = [];
  listaClientes: Cliente[] = [];
  selecionado: Venda;

  constructor(
    private router: Router,
    private vendaService: VendaService,
    private clienteService: ClienteService
  ) { }

  ngOnInit(): void {
    this.clienteService.pesquisar('').subscribe(
      data => {
        this.listaClientes = <Cliente[]>data;
      }
    );

    let codigoCliente = '';
    if(this.venda.cliente != null){
      codigoCliente = this.venda.cliente.codigo;
    }

    this.vendaService.pesquisar(codigoCliente).subscribe(
      data => {
        this.listaVendas = <Venda[]>data;

      }
    );
  }

  pesquisar(){
    let codigoCliente = '';
    if(this.venda.cliente != null){
      codigoCliente = this.venda.cliente.codigo;
    }

    this.vendaService.pesquisar(codigoCliente).subscribe(
      data => {
        this.listaVendas = <Venda[]>data;
      }
    );
  }

  incluir(){
    this.router.navigate(['/venda/incluir']);
  }

  remover(){

    this.vendaService.remover(this.selecionado).subscribe(
      data => {
        alert(data['mensagem']);
      }
    );
  }

  selecionar(valor){
    this.selecionado = valor;
  }

}
