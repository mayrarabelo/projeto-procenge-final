import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxMaskModule } from 'ngx-mask'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ProdutoComponent } from './produto/produto.component';
import { VendaComponent } from './venda/venda.component';
import { CadastroClienteComponent } from './cliente/cadastro-cliente/cadastro-cliente.component';
import { CadastroProdutoComponent } from './produto/cadastro-produto/cadastro-produto.component';
import { CadastroVendaComponent } from './venda/cadastro-venda/cadastro-venda.component';

@NgModule({
    declarations: [
        AppComponent,
        LayoutComponent,
        HomeComponent,
        ClienteComponent,
        ProdutoComponent,
        VendaComponent,
        CadastroClienteComponent,
        CadastroProdutoComponent,
        CadastroVendaComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        NgxMaskModule.forRoot(),

    ],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule { }
