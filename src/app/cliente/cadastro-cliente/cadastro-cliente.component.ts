import { Component, OnInit } from '@angular/core';
import { Cliente } from '../servico/cliente';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteService } from '../servico/cliente.service';

@Component({
    selector: 'app-cadastro-cliente',
    templateUrl: './cadastro-cliente.component.html',
    styleUrls: ['./cadastro-cliente.component.css']
})
export class CadastroClienteComponent implements OnInit {

    cliente: Cliente = new Cliente();
    ativacao: string = "";
    operacao: string = 'Incluir';

    listaUF: string[] = ["AC", "CE", "PE", "MA"];

    listaCidades: any[] = [
      {codigo: "1", nome:"Recife"},
      {codigo: "2", nome:"Vitória"},
      {codigo: "3", nome:"Piedade"},
      {codigo: "4", nome:"Jaboatão"}
    ];

    constructor(
        private router: Router,
        private rotaAtivada: ActivatedRoute,
        private clienteService: ClienteService
    ) { }

    ngOnInit(): void {
      this.verificar();
    }

    verificar(){
      this.ativacao = this.rotaAtivada.snapshot.params.id;
      if(this.ativacao != null){
          this.operacao = 'Alterar';
          this.clienteService.pesquisar(this.ativacao).subscribe(
              data => {
                  this.cliente = (<Cliente[]>data)[0];
              }
          )
      }
    }

    incluir(){
        this.clienteService.incluir(this.cliente).subscribe(
            data => {
                alert(data['mensagem']);
                this.voltar();
            }
        );
    }

    alterar(){
      this.clienteService.alterar(this.cliente).subscribe(
        data => {
          alert(data['mensagem']);
          this.voltar();
        }
      )
    }

    voltar(){
      this.router.navigate(['/cliente']);
    }

}
