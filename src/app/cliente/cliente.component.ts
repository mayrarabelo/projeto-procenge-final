import { Component, OnInit } from '@angular/core';
import { Cliente } from './servico/cliente';
import { Router } from '@angular/router';
import { ClienteService } from './servico/cliente.service';

@Component({
    selector: 'app-cliente',
    templateUrl: './cliente.component.html',
    styleUrls: ['./cliente.component.css']
})

export class ClienteComponent implements OnInit {

    cliente: Cliente = new Cliente();
    listaClientes: Cliente[] = [];
    selecionado: Cliente;

    constructor(
        private router: Router,
        private clienteService: ClienteService
    ) { }

    ngOnInit(): void {
        this.pesquisar();
    }


    pesquisar(){
        this.clienteService.pesquisar(this.cliente.nome).subscribe(
            data => {
                this.listaClientes = <Cliente[]>data;
            }
        );
    }

    incluir(){
        this.router.navigate(['/cliente/incluir']);
    }

    alterar(){
      this.router.navigate(['/cliente/alterar/'+this.selecionado.nome]);
    }

    remover(){
      this.clienteService.remover(this.selecionado).subscribe(
        data => {
          alert(data['mensagem']);
        }
      )
    }

    selecionar(valor){
      this.selecionado = valor;
    }

}
