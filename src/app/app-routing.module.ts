import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ProdutoComponent } from './produto/produto.component';
import { VendaComponent } from './venda/venda.component';
import { CadastroClienteComponent } from './cliente/cadastro-cliente/cadastro-cliente.component';
import { CadastroProdutoComponent } from './produto/cadastro-produto/cadastro-produto.component';
import { CadastroVendaComponent } from './venda/cadastro-venda/cadastro-venda.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'cliente',
    component: ClienteComponent,
    pathMatch: 'full'
  },
  {
    path: 'cliente/incluir',
    component: CadastroClienteComponent,
    pathMatch: 'full'
  },
  {
    path: 'cliente/alterar/:id',
    component: CadastroClienteComponent,
    pathMatch: 'full'
  },
  {
    path: 'produto',
    component: ProdutoComponent,
    pathMatch: 'full'
  },
  {
    path: 'produto/incluir',
    component: CadastroProdutoComponent,
    pathMatch: 'full'
  },
  {
    path: 'produto/alterar/:id',
    component: CadastroProdutoComponent,
    pathMatch: 'full'
  },
  {
    path: 'venda',
    component: VendaComponent,
    pathMatch: 'full'
  },
  {
    path: 'venda/incluir',
    component: CadastroVendaComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
