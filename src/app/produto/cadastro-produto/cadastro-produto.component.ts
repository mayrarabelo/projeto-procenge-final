import { Component, OnInit } from '@angular/core';
import { Produto } from '../servico/produto';
import { ActivatedRoute, Router } from '@angular/router';
import { ProdutoService } from '../servico/produto.service';

@Component({
  selector: 'app-cadastro-produto',
  templateUrl: './cadastro-produto.component.html',
  styleUrls: ['./cadastro-produto.component.css']
})
export class CadastroProdutoComponent implements OnInit {

    produto: Produto = new Produto();
    ativacao: string = "";
    operacao: string = 'Incluir';

    constructor(
        private router: Router,
        private rotaAtivada: ActivatedRoute,
        private produtoService: ProdutoService
    ) { }

    ngOnInit(): void {
      this.verificar();
    }

    verificar(){
      this.ativacao = this.rotaAtivada.snapshot.params.id;
      if(this.ativacao != null){
          this.operacao = 'Alterar';
          this.produtoService.pesquisar(this.ativacao).subscribe(
              data => {
                  this.produto = (<Produto[]>data)[0];
              }
          )
      }
    }

    incluir(){
        this.produtoService.incluir(this.produto).subscribe(
            data => {
                alert(data['mensagem']);
                this.voltar();
            }
        );
    }

    alterar(){
      this.produtoService.alterar(this.produto).subscribe(
        data => {
          alert(data['mensagem']);
          this.voltar();
        }
      )
    }

    voltar(){
      this.router.navigate(['/produto']);
    }

}
